FROM ubuntu:20.04
LABEL maintainer="itnmunoz@gmail.com"
RUN apt update && apt install -y openjdk-11-jdk
ENV JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64 \
    CATALINA_BASE=/etc/tomcat9 \
    CATALINA_HOME=/usr/share/tomcat9
ENV PATH $CATALINA_HOME/bin:$PATH
RUN apt install -y tomcat9 tomcat9-admin
RUN mkdir /etc/tomcat9/logs \
 && mkdir /etc/tomcat9/temp \
 && ln -s /etc/tomcat9 /etc/tomcat9/conf
COPY conf $CATALINA_BASE
COPY webapps $CATALINA_BASE/webapps
EXPOSE 8080
CMD ["catalina.sh", "run"]
